// axios
import axios from 'axios'

const domain = 'http://localhost:4000/'

export default axios.create({
  domain
  // You can add your headers here
})
