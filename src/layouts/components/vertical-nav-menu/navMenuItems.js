
export default [
  {
    url: "/dashboard",
    name: "Dashboard",
    slug: "dashboard",
    icon: "HomeIcon",
    i18n: "Dashboard",
  },
  {
    url: null,
    name: "Genealogy",
    icon: "PieChartIcon",
    i18n: "Genealogy",
    submenu: [
      {
        url: '/genealogy/view',
        name: "Graphical Genealogy",
        slug: "Graphical Genealogy",
        i18n: "Graphical Genealogy",
      },
      {
        url: '/tabulargenealogy/view',
        name: "Tabular Genealogy",
        slug: "Tabular Genealogy",
        i18n: "Tabular Genealogy",
      },
    ]
  },
  {
    url: null,
    name: "Compensation",
    icon: "ArchiveIcon",
    i18n: "Compensation",
    submenu: [
		{
        url: '/compensation/plans',
        name: "Plans",
        slug: "plans",
        i18n: "Plans",
      },
      { url: '/compensation/rank',
        name: "Rank",
        slug: "Rank",
        i18n: "Rank",  },
		{
		url: '/compensation/productlevelcommission',
        name: "Product Level Commission",
        slug: "Product Level Commission",
        i18n: "Product Level Commission",
      },
    ]
  },
  {
    url: null,
    name: "Usermanager",
    icon: "UserIcon",
    i18n: "Usermanager",
    submenu: [
      {
        url: '/usermanager/users',
        name: "Users",
        slug: "Users",
        i18n: "Users",
      },
		{
        url: '/usermanager/adduser',
        name: "Add User",
        slug: "users",
        i18n: "users",
      },
		 
    ]
  },
	{
    url: null,
    name: "Shopify",
    icon: "ShoppingBagIcon",
    i18n: "Shopify",
    submenu: [
      {
        url: '/shopify/shopifyconnection',
        name: "Shopify",
        slug: "Shopify",
        i18n: "Shopify",
      },
      {
        url: '/shopify/shopifyinstal',
        name: "Shopify Install",
        slug: "users",
        i18n: "users",
      },
    ]
  },
	{
    url: null,
    name: "Orders",
    icon: "ShoppingBagIcon",
    i18n: "Oders",
    submenu: [
      {
        url: '/order/createorder',
        name: "Create Order",
        slug: "order",
        i18n: "order",
      },		
		{
        url: '/order/orderhistory',
        name: "Order History",
        slug: "order",
        i18n: "order",
      },
		{
        url: '/order/saleshistory',
        name: "Product Sales History",
        slug: "order",
        i18n: "order",
      },
    ]
  },
  {
    url: null,
    name: "Store & Delivery",
    icon: "shoppingCartIcon",
    i18n: "Wallet",
    submenu: [   
		{
        url: '/category/estorecategory',
        name: "Category",
        slug: "Category",
        i18n: "Category",
      }, 
		{
        url: '/products/view',
        name: "View Products",
        slug: "products",
        i18n: "products",
      },
      {
        url: '/products/add',
        name: "Add Physical Products",
        slug: "addphysicalproducts",
        i18n: "addphysicalproducts",
      },
       {
        url: '/products/adddigital',
        name: "Add Digital Products",
        slug: "adddigitalproducts",
        i18n: "adddigitalproducts",
      },
		{
        url: '/giftcards/view',
        name: "Gift Cards",
        slug: "giftcards",
        i18n: "giftcards",
      },
		{
        url: '/attributes/view',
        name: "Attributes",
        slug: "attributes",
        i18n: "attributes",
      },
    ]
  },
  {
    url: null,
    name: "Wallet",
    icon: "CreditCardIcon",
    i18n: "Wallet",
    submenu: [
      {
        url: '/wallet/withdraw',
        name: "Withdraw",
        slug: "withdraw",
        i18n: "withdraw",
      },
      {
        url: '/wallet/ewallet',
        name: "E-Wallet",
        slug: "ewallet",
        i18n: "ewallet",
      },
      {
        url: '/bonus/bounslist',
        name: "Send Bonus",
        slug: "Send Bonus",
        i18n: "Send Bonus",
      },
      {
        url: '/bonus/detectbonuslist',
        name: "Deduct Bonus",
        slug: "Deduct Bonus",
        i18n: "Deduct Bonus",
      },
    ]
  },
  {
    url: null,
    name: "Reports",
    icon: "FileIcon",
    i18n: "Wallet",
    submenu: [
      {
        url: '/reports/adminearnings',
        name: "Admin Earnings",
        slug: "Admin Earnings",
        i18n: "Admin Earnings",
      },
      {
        url: '/reports/distributors',
        name: "Distributors",
        slug: "Distributors",
        i18n: "Distributors",
      },
      {
        url: '/reports/subscription',
        name: "Subscription",
        slug: "Subscription",
        i18n: "Subscription",
      },
      {
        url: '/reports/pv',
        name: "PV",
        slug: "pv",
        i18n: "pv",
      },
      {
        url: '/reports/gpv',
        name: "GPV",
        slug: "gpv",
        i18n: "gpv",
      },
		 {
        url: '/reports/usercommisssion',
        name: "User Commisssion",
        slug: "User Commisssion",
        i18n: "User Commisssion",

      },
      
      {
        url: '/reports/totalcommission',
        name: "Total Commission",
        slug: "Total Commission",
        i18n: "Total Commission",

      },
    ]
  },
	{
    url: null,
    name: "CMS",
    icon: "GridIcon",
    i18n: "CMS",
    submenu: [
      {
        url: '/cms/contentmanagement',
        name: "Content Management",
        slug: "contentmanagementff",
        i18n: "contentmanagement",
      },   
                                            
    ]
  },
	 {
    url: null,
    name: "Coaching",
    icon: "ListIcon",
    i18n: "Coaching",
    submenu: [
      {
        url: '/coaching/managecoaching',
        name: "Manage Coaching",
        slug: "coaching",
        i18n: "coaching",
      },
    
    ]
  },
	 {
    url: null,
    name: "E-Pin",
    icon: "PaperclipIcon",
    i18n: "E-Pin",
    submenu: [
      {
        url: '/epin/sendepin',
        name: "Send E-pin",
        slug: "sendepin",
        i18n: "sendepin",
      }, 
      {
        url: '/epin/epinmanagement',
        name: "E-Pin Management",
        slug: "epinmanagement",
        i18n: "epinmanagement",
      },    
                                            
    ]
  },
  {
    url: null,
    name: "Settings",
    icon: "SettingsIcon",
    i18n: "Settings",
    submenu: [
      {
        url: '/settings/sitesettings',
        name: "Site",
        slug: "Site",
        i18n: "Site",
      },   
      {
        url: '/settings/paymentsettings',
        name: "Payment Settings",
        slug: "Payment Settings",
        i18n: "Payment Settings",
      },
      {
        url: '/settings/payoutsettings',
        name: "Payout Settings",
        slug: "Payout Settings",
        i18n: "Payout Settings",
      },
      {
        url: '/settings/payoutconfig',
        name: "Payout Configuration",
        slug: "Payout Configuration",
        i18n: "Payout Configuration",
      },
      {
        url: '/settings/currencysettings',
        name: "Currency Settings",
        slug: "Currency Settings",
        i18n: "Currency Settings",
      },
      {
        url: '/settings/mailsettings',
        name: "Mail Settings",
        slug: "Mail Settings",
        i18n: "Mail Settings",
      }, 
      {
        url: '/settings/registersetting',
        name: "Registration Settings",
        slug: "Registration Settings",
        i18n: "Registration Settings",
      },                                       
    ]
  },
  
  
]
