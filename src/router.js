/*=========================================================================================
  File Name: router.js
  Description: Routes for vue-router. Lazy loading is enabled.
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    scrollBehavior () {
        return { x: 0, y: 0 }
    },
    routes: [

        {
    // =============================================================================
    // MAIN LAYOUT ROUTES
    // =============================================================================
            path: '',
            component: () => import('./layouts/main/Main.vue'),
            meta: {
              rule: 'editor',
              adminOnly: true
            },
            children: [
        // =============================================================================
        // Theme Routes
        // =============================================================================
              {
                path: '/dashboard',
                name: 'home',
                component: () => import('@/views/Home.vue'),
                meta: {
                  rule: 'editor',
                  adminOnly: true
                }
              },
              {
                path: '/genealogy/view/:matrix_id',
                name: 'genealogy',
                component: () => import('@/views/pages/genealogy/genealogy.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Genealogy'},
                    { title: 'Graphical Genealogy', active: true },

                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
				  
              {
                path: '/tabulargenealogy/view',
                name: 'tabulargenealogy',
                component: () => import('@/views/pages/tabulargenealogy/tabulargenealogy.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Genealogy'},
                    { title: 'Tabular Genealogy', active: true },

                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              {
                path: '/compensation/plans',
                name: 'plans',
                component: () => import('@/views/pages/compensation/plans.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Compensation'},
                    { title: 'Plans', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              {
                path: '/compensation/addplan',
                name: 'app-plan',
                component: () => import('@/views/pages/compensation/addplan.vue'),
                meta: {
                    breadcrumb: [
                        { title: 'Home', url: '/dashboard' },
                        { title: 'Compensation', url: '/compensation/plans' },
                        { title: 'Add Plan', active: true },
                    ],
                    rule: 'editor',
                    adminOnly: true
                }
              },
              {
                path: '/compensation/plans/configplan/:plan_id',
                name: 'plans',
                component: () => import('@/views/pages/compensation/configplan.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Compensation'},
                    { title: 'Plans', active: true, url: '/compensation/plans'},
                    { title: 'Configplan', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              {
                path: '/compensation/rank',
                name: 'rank',
                component: () => import('@/views/pages/compensation/rank/rank.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Compensation'},
                    { title: 'Rank', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              {
                path: '/compensation/rank/addrank',
                name: 'addrank',
                component: () => import('@/views/pages/compensation/rank/addrank.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Compensation'},
                    { title: 'Add Rank', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              {
                path: '/compensation/rank/editrank/:rank_id',
                name: 'addrank',
                component: () => import('@/views/pages/compensation/rank/editrank.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Compensation'},
                    { title: 'Edit Rank', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
        {
                path: '/compensation/productlevelcommission',
                name: 'productlevelcommission',
                component: () => import('@/views/pages/compensation/productlevelcommission/levelcommission.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Compensation'},
                    { title: 'Product Level Commission', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              {
                path: '/compensation/productlevelcommission/add',
                name: 'productlevelcommission',
                component: () => import('@/views/pages/compensation/productlevelcommission/addlevelcommission.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Compensation'},
                    { title: 'Add Product Level Commission', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              {
                path: '/compensation/productlevelcommission/edit/:productlevelcommission_id',
                name: 'addrank',
                component: () => import('@/views/pages/compensation/productlevelcommission/editlevelcommission.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Compensation'},
                    { title: 'Edit Product Level Commission', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              { 
                path: '/usermanager/users',
                name: 'Users',
                component: () => import('@/views/pages/users/userlist.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Usermanager'},
                    { title: 'Users', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
				  
				   {
                path: '/usermanager/adduser',
                name: 'adduser',
                component: () => import('./views/pages/users/adduser.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Usermanager'},
                    { title: 'Add User', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
				  
				  {
                path: '/coaching/managecoaching',
                name: 'app-coaching-manage',
                component: () => import('@/views/pages/coaching/managecoaching.vue'),
                meta: {
                    breadcrumb: [
                        { title: 'Home', url: '/dashboard' },
                        { title: 'Coaching' },
                        { title: 'Manage Courses', active: true },
                    ],
                    rule: 'editor'
                },
            },

            {
              path: '/coaching/addcourse',
              name: 'app-coaching-manage',
              component: () => import('@/views/pages/coaching/addcourse.vue'),
              meta: {
                  breadcrumb: [
                      { title: 'Home', url: '/dashboard' },
                      { title: 'Coaching' },
                      { title: 'Manage Courses', active: true },
                  ],
                  rule: 'editor'
              },
          },

          {
            path: '/coaching/addlesson/:course_id',
            name: 'app-coaching-manage',
            component: () => import('@/views/pages/coaching/addlesson.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Coaching',url: '/coaching/managecoaching' },
                    { title: 'Add Lession', active: true },
                ],
                rule: 'editor'
            },
        },

        {
          path: '/coaching/editlesson/:courses_lesson_id',
          name: 'Course',
          component: () => import('./views/pages/coaching/editlesson.vue'),
          meta: {
            breadcrumb: [
              { title: 'Home', url: '/dashboard' },
              { title: 'Coaching'},
              { title: 'Edit Course', active: true },
            ],
            rule: 'editor',
            adminOnly: true
          }
        },

          {
            path: '/coaching/editcourse/:course_id',
            name: 'Course',
            component: () => import('./views/pages/coaching/editcourse.vue'),
            meta: {
              breadcrumb: [
                { title: 'Home', url: '/dashboard' },
                { title: 'Coaching'},
                { title: 'Edit Course', active: true },
              ],
              rule: 'editor',
              adminOnly: true
            }
          },
          {
            path: '/coaching/viewlessone/:courses_lesson_id',
            name: 'Course',
            component: () => import('./views/pages/coaching/viewlessone.vue'),
            meta: {
              breadcrumb: [
                { title: 'Home', url: '/dashboard' },
                { title: 'Coaching'},
                { title: 'view Lesson', active: true },
              ],
              rule: 'editor',
              adminOnly: true
            }
          },
          {
            path: '/coaching/viewcourse/:course_id',
            name: 'Course',
            component: () => import('./views/pages/coaching/viewcourse.vue'),
            meta: {
              breadcrumb: [
                { title: 'Home', url: '/dashboard' },
                { title: 'Coaching'},
                { title: 'View Lesson', active: true },
              ],
              rule: 'editor',
              adminOnly: true
            }
          },
				  
              {
                path: '/usermanager/userprofile/:members_id',
                name: 'users',
                component: () => import('@/views/pages/users/userprofile.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Usermanager'},
                    { title: 'User Profile', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              {
                path: '/usermanager/edituser/:members_id',
                name: 'users',
                component: () => import('@/views/pages/users/edituser.vue'),
                meta: { 
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Usermanager', url: '/usermanager/users'},
                    { title: 'Edit User', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },{
                path: '/category/estorecategory',
                name: 'Categories',
                component: () => import('@/views/pages/estore/EstoreCategory.vue'),
                meta: {
                  rule: 'editor',
                  adminOnly: true
                }
              },
				  
			{
                path: '/subadmin',
                name: 'Role Management',
                component: () => import('@/views/pages/rolemanagement/viewrolemanagement.vue'),
                meta: {
					breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Usermanager'},
                    { title: 'Role Management', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              {
                path: '/addsubadmin',
                name: 'Add Role Management',
                component: () => import('@/views/pages/rolemanagement/addrolemanagement.vue'),
                meta: {
					breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Usermanager'},
                    { title: 'Add Role Management', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              {
                path: '/subadmin/edit/:id',
                name: 'Role Management',
                component: () => import('@/views/pages/rolemanagement/editrolemanagement.vue'),
                meta: {
					breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Usermanager'},
                    { title: 'Edit Role Management', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              {
                path: '/category/addcategory',
                name: 'Add Category',
                component: () => import('@/views/pages/estore/AddCategory.vue'),
                meta: {
                  rule: 'editor',
                  adminOnly: true
                }
              },
				  {
                path: '/category/editcategory/:category_id',
                name: 'Edit Category',
                component: () => import('@/views/pages/estore/EditCategory.vue'),
                meta: {
                  rule: 'editor',
                  adminOnly: true
                }
              },
				{
              path: '/products/view',
              name: 'app-products',
              component: () => import('@/views/pages/products/ViewProducts.vue'),
              meta: {
                breadcrumb: [
                  { title: 'Home', url: '/dashboard' }, 
				  { title: 'Store & Delivery'},
                  { title: 'View Products', active: true },
              ],
                  rule: 'editor',
                  adminOnly: true
              }
            },
            {
                path: '/products/add',
                name: 'app-products',
                component: () => import('@/views/pages/products/AddProducts.vue'),
                meta: {
                    breadcrumb: [
                        { title: 'Home', url: '/dashboard' },
                        { title: 'View Products', url: '/products/view' },
                        { title: 'Add Physical Products', active: true },
                    ],
                    rule: 'editor',
                    adminOnly: true
                }
              },
              {
                path: '/products/edit/:product_id',
                name: 'app-products-edit',
                component: () => import('@/views/pages/products/EditProducts.vue'),
                meta: {
                    breadcrumb: [
                        { title: 'Home', url: '/dashboard' },
                        { title: 'View Products', url: '/products/view' },
                        { title: 'Edit Products', active: true },
                    ],
                    rule: 'editor',
                    adminOnly: true
                }
              },
        {
              path: '/products/adddigital',
              name: 'app-products',
              component: () => import('@/views/pages/products/AddDigitalProducts.vue'),
              meta: {
                  breadcrumb: [
                      { title: 'Home', url: '/dashboard' },
                      { title: 'View Products', url: '/products/view' },
                      { title: 'Add Digital Products', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
              }
            },
            {
              path: '/products/editdigital/:product_id',
              name: 'app-products-edit',
              component: () => import('@/views/pages/products/EditDigitalProducts.vue'),
              meta: {
                  breadcrumb: [
                      { title: 'Home', url: '/dashboard' },
                      { title: 'View Products', url: '/products/view' },
                      { title: 'Edit Products', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
              }
            },
              {
                path: '/order/createorder',
                name: 'app-order-create',
                component: () => import('@/views/pages/order/CreateOrder.vue'),
                meta: {
                    breadcrumb: [
                        { title: 'Home', url: '/dashboard' },
                        { title: 'Order' },
                        { title: 'Create Order', active: true },
                    ],
                    rule: 'editor'
                },
            },        
      {
              path: '/order/orderhistory',
              name: 'app-order-create',
              component: () => import('@/views/pages/order/OrdersHistroy.vue'),
              meta: {
                  breadcrumb: [
                      { title: 'Home', url: '/dashboard' },
                      { title: 'Order' },
                      { title: 'Order History', active: true }, 
                  ],
                  rule: 'editor',
                  adminOnly: true
              },
          },
          {
            path: '/order/orderdetails/:orderid',
            name: 'app-order-details',
            component: () => import('@/views/pages/order/ViewOrderDetails.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Order' },
                    { title: 'Order History'},
                    { title: 'View Order', active: true },
                ],
                rule: 'editor',
                adminOnly: true
            },
        },
      {
            path: '/order/saleshistory',
            name: 'app-order-create',
            component: () => import('@/views/pages/order/ProductSalesHistory.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Order' },
                    { title: 'Product Sales History', active: true },
                ],
                rule: 'editor',
                adminOnly: true
            },
        },
         {
                path: '/wallet/withdraw',
                name: 'withdraw',
                component: () => import('@/views/pages/wallet/withdrawal/withdraw.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Wallet'},
                    { title: 'Withdraw', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              {
                path: '/wallet/ewallet',
                name: 'withdraw',
                component: () => import('@/views/pages/wallet/E-wallet/ewallet.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Wallet'},
                    { title: 'E-Wallet', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              {
                path: '/bonus/bounslist',
                name: 'bonus',
                component: () => import('@/views/pages/wallet/sendbonus/bouns.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Wallet'},
                    { title: 'Send Bonus', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              {
                path: '/wallet/bonus/sentbouns',
                name: 'sent-bonus',
                component: () => import('@/views/pages/wallet/sendbonus/sentbouns.vue'),
                meta: {
                    breadcrumb: [
                        { title: 'Home', url: '/dashboard' },
                        { title: 'Wallet', url: '/wallet/bonus/sentbouns' },
                        { title: 'Send Bonus', active: true },
                    ],
                    rule: 'editor',
                    adminOnly: true
                }
              },
              {
                path: '/bonus/detectbonuslist',
                name: 'bonus',
                component: () => import('@/views/pages/wallet/deductbonus/detectbonus.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Wallet'},
                    { title: 'Deduct Bonus', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              {
                path: '/wallet/bonus/sentdetectbonus',
                name: 'sent-bonus',
                component: () => import('@/views/pages/wallet/deductbonus/sentdetectbonus.vue'),
                meta: {
                    breadcrumb: [
                        { title: 'Home', url: '/dashboard' },
                        { title: 'Wallet', url: '/wallet/bonus/sentdetectbonus' },
                        { title: 'Deduct Bonus', active: true },
                    ],
                    rule: 'editor',
                    adminOnly: true
                }
              },
				{
                path: '/settings/integration',
                name: 'settings',
                component: () => import('@/views/pages/integration/integration.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Settings'},
                    { title: 'Integration', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              {
                path: '/settings/easypost',
                name: 'settings',
                component: () => import('@/views/pages/integration/easypost.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Settings'},
                    { title: 'Integration', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
			  {
                path: '/settings/tax',
                name: 'settings',
                component: () => import('@/views/pages/settings/Tax.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Settings'},
                    { title: 'Tax Configuration', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              {
                path: '/settings/paymentsettings',
                name: 'settings',
                component: () => import('@/views/pages/settings/Paymentsettings.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Settings'},
                    { title: 'Payment Settings', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              {
                path: '/settings/sitesettings',
                name: 'settings',
                component: () => import('@/views/pages/settings/Sitesettings.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Settings'},
                    { title: 'Site Settings', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              {
                path: '/settings/payoutsettings',
                name: 'settings',
                component: () => import('@/views/pages/settings/Payoutsettings.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Settings'},
                    { title: 'Payout Settings', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },            
              {
                path: '/settings/payoutconfig',
                name: 'settings',
                component: () => import('@/views/pages/settings/Payoutconfig.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Settings'},
                    { title: 'Payout Configuration', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              }, 
              {
                path: '/settings/currencysettings',
                name: 'settings',
                component: () => import('@/views/pages/settings/CurrencySettings.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Settings'},
                    { title: 'Currency Settings', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              }, 
              {
                path: '/settings/mailsettings',
                name: 'settings',
                component: () => import('@/views/pages/settings/MailSettings.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Settings'},
                    { title: 'Mail Settings', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },                         
              {
                path: '/settings/registersetting',
                name: 'settings',
                component: () => import('@/views/pages/settings/RegisterSetting.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Settings'},
                    { title: 'Registration Settings', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              {
                path: '/reports/adminearnings',
                name: 'adminearnings',
                component: () => import('@/views/pages/reports/AdminEarnings/adminearnings.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Reports'},
                    { title: 'Admin Earnings', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              {
                path: '/reports/distributors',
                name: 'distributors',
                component: () => import('@/views/pages/reports/Distributors/distributors.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Reports'},
                    { title: 'Distributors', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
			{
                path: '/reports/customers',
                name: 'customers',
                component: () => import('@/views/pages/reports/Distributors/customers.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Reports'},
                    { title: 'Customers', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              {
                path: '/reports/subscription',
                name: 'subscription',
                component: () => import('@/views/pages/reports/Subscription/subscription.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Reports'},
                    { title: 'Subscription', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              {
                path: '/reports/pv',
                name: 'pv',
                component: () => import('@/views/pages/reports/PV/pv.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Reports'},
                    { title: 'PV', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              {
                path: '/reports/gpv',
                name: 'pv',
                component: () => import('@/views/pages/reports/GPV/gpv.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Reports'},
                    { title: 'GPV', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
				   {
                path: '/reports/usercommisssion',
                name: 'usercommisssion',
                component: () => import('./views/pages/reports/UserCommission/usercommisssion.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Reports'},
                    { title: 'UserCommisssion', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
              
              {
                path: '/reports/totalcommission',
                name: 'totalcommission',
                component: () => import('@/views/pages/reports/TotalCommission/totalcommission.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Reports'},
                    { title: 'Total Commission', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
				   {
            path: '/reports/review',
            name: 'review',
            component: () => import('@/views/pages/reports/review/productreviewlist.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'Reports' },
                    { title: 'Products', active: true }, 
                ],
                rule: 'editor',
                adminOnly: true
            },
        },
        {
          path: '/reports/productreviewlist/:reviewid',
          name: 'review',
          component: () => import('@/views/pages/reports/review/reviewlist.vue'),
          meta: {
              breadcrumb: [
                { title: 'Home', url: '/dashboard' },
                { title: 'Reports' },
                { title: 'Products'}, 
                { title: 'Review', active: true }, 
              ],
              rule: 'editor',
              adminOnly: true
          },
      },
      {
        path: '/reports/reviewdetails/:id',
        name: 'review',
        component: () => import('@/views/pages/reports/review/reviewdetails.vue'),
        meta: {
            breadcrumb: [
              { title: 'Home', url: '/dashboard' },
              { title: 'Reports' },
              { title: 'Products'}, 
              { title: 'Review'}, 
              { title: 'Review Detail', active: true }, 
            ],
            rule: 'editor',
            adminOnly: true
        },
    },
              {
                path: '/apps/eCommerce/shop',
                name: 'ecommerce-shop',
                component: () => import('@/views/apps/eCommerce/ECommerceShop.vue'),
                meta: {
                    breadcrumb: [
                        { title: 'Home', url: '/dashboard' },
                        { title: 'eCommerce'},
                        { title: 'Shop', active: true },
                    ],
                    pageTitle: 'Shop',
                    rule: 'editor'
                }
            },//sankar start
				 {
              path: '/attributes/view',
              name: 'app-attributes',
              component: () => import('@/views/pages/attributes/ViewAttributes.vue'),
              meta: {
                  breadcrumb: [
                      { title: 'Home', url: '/dashboard' },
					  { title: 'Store & Delivery'},
                      { title: 'View Attributes', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
              }
            },
			{
              path: '/giftcards/view',
              name: 'app-giftcards',
              component: () => import('@/views/pages/giftcards/ViewGiftcards.vue'),
              meta: {
                  breadcrumb: [
                      { title: 'Home', url: '/dashboard' },
                      { title: 'View Gift cards', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
              }
            },
            {
              path: '/giftcards/details/:giftcards_id',
              name: 'carddetails',
              component: () => import('@/views/pages/giftcards/carddetails.vue'),
              meta: {
                breadcrumb: [
                  { title: 'Home', url: '/dashboard' },
                  { title: 'Card Details', active: true },
                ],
                rule: 'editor',
                adminOnly: true
              }
            },	
            {
              path: '/configure/view/:attr_id',
              name: 'app-attributes',
              component: () => import('@/views/pages/attributes/ViewAttributesConfigure.vue'),
              meta: {
                  breadcrumb: [
                      { title: 'Home', url: '/dashboard' },
                      { title: 'View Attributes Configure', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
              }
            },//sankar end
            {
                path: '/apps/eCommerce/wish-list',
                name: 'ecommerce-wish-list',
                component: () => import('@/views/apps/eCommerce/ECommerceWishList.vue'),
                meta: {
                    breadcrumb: [
                        { title: 'Home', url: '/' },
                        { title: 'eCommerce', url:'/apps/eCommerce/shop'},
                        { title: 'Wish List', active: true },
                    ],
                    pageTitle: 'Wish List',
                    rule: 'editor'
                }
            },
            {
                path: '/apps/eCommerce/checkout',
                name: 'ecommerce-checkout',
                component: () => import('@/views/apps/eCommerce/ECommerceCheckout.vue'),
                meta: {
                    breadcrumb: [
                        { title: 'Home', url: '/dashboard' },
                        { title: 'eCommerce', url:'/apps/eCommerce/shop'},
                        { title: 'Checkout', active: true },
                    ],
                    pageTitle: 'Checkout',
                    rule: 'editor'
                }
            },
            /*
              Below route is for demo purpose
              You can use this route in your app
                {
                    path: '/apps/eCommerce/item/',
                    name: 'ecommerce-item-detail-view',
                    redirect: '/apps/eCommerce/shop',
                }
            */
            {
                path: '/apps/eCommerce/item/',
                redirect: '/apps/eCommerce/item/5546604',
            },
            {
                path: '/apps/eCommerce/item/:item_id',
                name: 'ecommerce-item-detail-view',
                component: () => import('@/views/apps/eCommerce/ECommerceItemDetailView.vue'),
                meta: {
                    breadcrumb: [
                        { title: 'Home', url: '/dashboard' },
                        { title: 'eCommerce'},
                        { title: 'Shop', url: {name: 'ecommerce-shop'} },
                        { title: 'Item Details', active: true },
                    ],
                    parent: "ecommerce-item-detail-view",
                    pageTitle: 'Item Details',
                    rule: 'editor'
                }
            },
            {
                path: '/apps/user/user-list',
                name: 'app-user-list',
                component: () => import('@/views/apps/user/user-list/UserList.vue'),
                meta: {
                    breadcrumb: [
                        { title: 'Home', url: '/dashboard' },
                        { title: 'User' },
                        { title: 'List', active: true },
                    ],
                    pageTitle: 'User List',
                    rule: 'editor'
                },
            },
            {
                path: '/apps/user/user-view/:userId',
                name: 'app-user-view',
                component: () => import('@/views/apps/user/UserView.vue'),
                meta: {
                    breadcrumb: [
                        { title: 'Home', url: '/dashboard' },
                        { title: 'User' },
                        { title: 'View', active: true },
                    ],
                    pageTitle: 'User View',
                    rule: 'editor'
                },
            },
            {
                path: '/apps/user/user-edit/:userId',
                name: 'app-user-edit',
                component: () => import('@/views/apps/user/user-edit/UserEdit.vue'),
                meta: {
                    breadcrumb: [
                        { title: 'Home', url: '/dashboard' },
                        { title: 'User' },
                        { title: 'Edit', active: true },
                    ],
                    pageTitle: 'User Edit',
                    rule: 'editor'
                },
            },{
              path: '/profile/changepassword',
              name: 'app-changepassword',
              component: () => import('@/views/pages/profile/ChangePassword.vue'),
              meta: {
                  breadcrumb: [
                      { title: 'Home', url: '/dashboard' },
                      { title: 'Change Password', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
              }
            },
			{
              path: '/profile/adminprofile',
              name: 'adminprofile',
              component: () => import('@/views/pages/profile/profile.vue'),
              meta: {
                  breadcrumb: [
                      { title: 'Home', url: '/dashboard' },
                      { title: 'Admin Profile', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
              }
            },
			{
              path: '/usermanager/genealogytree/:members_id/:matrix_id',
              name: 'users',
              component: () => import('@/views/pages/users/genealogytree.vue'),
              meta: {
                breadcrumb: [
                  { title: 'Home', url: '/dashboard' },
                  { title: 'Usermanager'},
                  { title: 'users', active: true },
                ],
                rule: 'editor',
                adminOnly: true
              }
            },
            {
              path: '/usermanager/genealogytab/:members_id/:matrix_id',
              name: 'users',
              component: () => import('@/views/pages/users/genealogytab.vue'),
              meta: {
                breadcrumb: [
                  { title: 'Home', url: '/dashboard' },
                  { title: 'Usermanager'},
                  { title: 'users', active: true },
                ],
                rule: 'editor',
                adminOnly: true
              }
            },
				{
                path: '/cms/contentmanagement',
                name: 'Content Management',
                component: () => import('@/views/pages/cms/contentmanagement.vue'),
                meta: {
                  breadcrumb: [
                    { title: 'Home', url: '/dashboard' },
                    { title: 'CMS'},
                    { title: 'Content Management', active: true },
                  ],
                  rule: 'editor',
                  adminOnly: true
                }
              },
				  {
              path: '/epin/sendepin',
              name: 'Send E-Pin',
              component: () => import('@/views/pages/epin/sendepin.vue'),
              meta: {
                breadcrumb: [
                  { title: 'Home', url: '/dashboard' },
                  { title: 'E-Pin'},
                  { title: 'Send E-Pin', active: true },
                ],
                rule: 'editor',
                adminOnly: true
              }
            },
				{
              path: '/epi/setting',
              name: 'Send E-Pin',
              component: () => import('@/views/pages/epin/epinsetting.vue'),
              meta: {
                breadcrumb: [
                  { title: 'Home', url: '/dashboard' },
                  { title: 'E-Pin'},
                  { title: 'E-Pin Setting', active: true },
                ],
                rule: 'editor',
                adminOnly: true
              }
            },
            {
              path: '/epin/epinmanagement',
              name: 'E-Pin Management',
              component: () => import('@/views/pages/epin/epinmanagement.vue'),
              meta: {
                breadcrumb: [
                  { title: 'Home', url: '/dashboard' },
                  { title: 'E-Pin'},
                  { title: 'E-Pin Management', active: true },
                ],
                rule: 'editor',
                adminOnly: true
              }
            }, {
                path: '/shopify/shopifyconnection',
                name: 'Shopify connection',
                component: () => import('@/views/pages/shopify/shopifyconnection.vue'),
                meta: {
                breadcrumb: [
                { title: 'Home', url: '/dashboard' },
                { title: 'Shopify'},
                { title: 'Shopify', active: true },
                ],
                rule: 'editor',
                adminOnly: true
                }
                },
        {
                path: '/shopify/shopifyinstal',
                name: 'Shopify connection',
                component: () => import('@/views/pages/shopify/shopifyinstalation.vue'),
                meta: {
                breadcrumb: [
                { title: 'Home', url: '/dashboard' },
                { title: 'Shopify'},
                { title: 'Shopify', active: true },
                ],
                rule: 'editor',
                adminOnly: true
                }
         },
		{
          path: '/tickets',
          name: 'Tickets',
          component: () => import('@/views/pages/tickets/ticketlist.vue'),
          meta: {
          breadcrumb: [
          { title: 'Home', url: '/dashboard' },
          { title: 'Tickets', active: true },
          ],
          rule: 'editor',
          adminOnly: true
          },},
          {
            path: '/tickets/details/:tickets_id',
            name: 'Tickets',
            component: () => import('@/views/pages/tickets/ticketdetails.vue'),
            meta: {
            breadcrumb: [
            { title: 'Home', url: '/dashboard' },
            { title: 'Tickets'},
            { title: 'View', active: true },
            ],
            rule: 'editor',
            adminOnly: true
            }
          },		
			 {
            path: '/helparticles',
            name: 'HelpArticles',
            component: () => import('@/views/pages/helparticles/helparticles.vue'),
            meta: {
            breadcrumb: [
            { title: 'Home', url: '/dashboard' },
            { title: 'Help Articles', active: true },
            ],
            rule: 'editor',
            adminOnly: true
            }
          },
          {
            path: '/helparticles/edit/:help_articles_id',
            name: 'EditHelpArticles',
            component: () => import('@/views/pages/helparticles/edithelparticles.vue'),
            meta: {
            breadcrumb: [
            { title: 'Home', url: '/dashboard' },
            { title: 'Help Articles', active: true },
            ],
            rule: 'editor',
            adminOnly: true
            }
          },
          {
            path: '/helparticles/add',
            name: 'HelpArticles',
            component: () => import('@/views/pages/helparticles/addhelparticles.vue'),
            meta: {
            breadcrumb: [
            { title: 'Home', url: '/dashboard' },
            { title: 'Help Articles'},
            { title: 'Add Help Articles', active: true },
            ],
            rule: 'editor',
            adminOnly: true
            }
          },
{
    path: '/wordpress/wordpressconnection',
    name: 'Wordpress connection',
    component: () => import('@/views/pages/wordpress/wordpressconnection.vue'),
    meta: {
    breadcrumb: [
    { title: 'Home', url: '/dashboard' },
    { title: 'Wordpress'},
    { title: 'Wordpress', active: true },
    ],
    rule: 'editor',
    adminOnly: true
    }
    },	
				
            ],
        },
    // =============================================================================
    // FULL PAGE LAYOUTS
    // =============================================================================
        {
            path: '',
            component: () => import('@/layouts/full-page/FullPage.vue'),
            children: [
        // =============================================================================
        // PAGES
        // =============================================================================
              {
                path: '/login',
                name: 'login',
                component: () => import('@/views/pages/login/Login.vue')
              },
              {
                path: '/pages/register',
                name: 'page-register',
                component: () => import('@/views/pages/register/Register.vue'),
                meta: {
                    rule: 'editor',
                }
              },
              {
                  path: '/pages/forgot-password',
                  name: 'page-forgot-password',
                  component: () => import('@/views/pages/ForgotPassword.vue'),
                  meta: {
                      rule: 'editor'
                  }
              },
              {
                  path: '/pages/reset-password',
                  name: 'page-reset-password',
                  component: () => import('@/views/pages/ResetPassword.vue'),
                  meta: {
                      rule: 'editor'
                  }
              },
              {
                  path: '/pages/lock-screen',
                  name: 'page-lock-screen',
                  component: () => import('@/views/pages/LockScreen.vue'),
                  meta: {
                      rule: 'editor'
                  }
              },
              {
                path: '/pages/error-404',
                name: 'page-error-404',
                component: () => import('@/views/pages/Error404.vue')
              },
				  
            ]
        },
        // Redirect to 404 page, if no match found
        {
            path: '*',
            redirect: '/pages/error-404'
        }
    ],
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.adminOnly)) {
     const loggedIn = localStorage.getItem('admin_token'); 
      if (loggedIn == null) {
        next('/login')
      } else {
        next();
      }
  } else {
     next();
    }
   
 });


router.afterEach(() => {
  // Remove initial loading
  const appLoading = document.getElementById('loading-bg')
    if (appLoading) {
        appLoading.style.display = "none";
    }
})

export default router
