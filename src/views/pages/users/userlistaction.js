
export default ({
    template: `
            <button radius @click="testclick" color="success" type="flat" icon-pack="feather" icon="icon-camera"></button>
    `,
    data: function () {
        return {
        };
    },
    beforeMount() {
    },
    mounted() {
    },
    methods: {
        testclick() {
            this.params.context.componentParent.methodFromParent(`Row: ${this.params.node.rowIndex}, Col: ${this.params.colDef.headerName}`)
        }
    }
});

// export  default {
//   components: {
//     AgGridVue
//   },
//   data() {
//     return {
//     }
//   },
//   watch: {
//   },
//   computed: {
//   },
//   methods: {
//       testclick() {
//           alert("Dsadsadas");
//       }
//   },
//   mounted() {

//   }
// }
